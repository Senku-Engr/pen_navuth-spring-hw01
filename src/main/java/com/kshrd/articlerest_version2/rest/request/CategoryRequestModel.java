package com.kshrd.articlerest_version2.rest.request;

public class CategoryRequestModel {
    private String title;

    public  CategoryRequestModel(){}

    public CategoryRequestModel(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "CategoryRequestModel{" +
                "title='" + title + '\'' +
                '}';
    }
}
