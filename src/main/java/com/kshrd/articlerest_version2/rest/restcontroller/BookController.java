package com.kshrd.articlerest_version2.rest.restcontroller;

import com.kshrd.articlerest_version2.repository.BookRepository;
import com.kshrd.articlerest_version2.repository.dto.Book;
import com.kshrd.articlerest_version2.repository.dto.BookDto;
import com.kshrd.articlerest_version2.rest.response.BaseApiResponse;
import com.kshrd.articlerest_version2.service.impl.BookServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class BookController {
    private BookRepository bookRepository;
    private BookServiceImpl bookService;

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Autowired
    public void setBookService(BookServiceImpl bookService) {
        this.bookService = bookService;
    }

    @PostMapping("/books")
    public ResponseEntity<BaseApiResponse<BookDto>> insert(
            @RequestBody BookDto bookDto) {
        BaseApiResponse<BookDto> response = new BaseApiResponse<>();
        try {
            BookDto result = bookService.insert(bookDto);
            response.setMessage("You have added Book successfully");
            response.setData(result);
            response.setStatus(HttpStatus.OK);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        } catch (Exception e) {

            response.setMessage(" add FAIL maybe DUPLICATE title or check your keyword : category_id.....");
            response.setData(null);
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);

        }

    }

    @DeleteMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<Book>> delete(@PathVariable("id") int id) {
        BaseApiResponse<Book> response = new BaseApiResponse();

        boolean isDeleted = bookService.delete(id);
        if (isDeleted) {
            response.setMessage("You have delete Book SUCCESSFULLY (id : " + id + ")");
            response.setData(null);
            response.setStatus(HttpStatus.OK);
            response.setTime(new Timestamp(System.currentTimeMillis()));

        } else {
            response.setMessage("You have delete Book FAILED (id : " + id + ")");
            response.setData(null);
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTime(new Timestamp(System.currentTimeMillis()));
        }

        return ResponseEntity.ok(response);

    }

    @PutMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<BookDto>> update(
            @RequestBody BookDto bookDto, @PathVariable("id") int id) {
        bookDto.setId(id);
        System.out.println(bookDto);

        System.out.println(id);
        BaseApiResponse<BookDto> response = new BaseApiResponse();
        try {
            BookDto isDeleted = bookService.update(bookDto, id);
            if (isDeleted != null) {
                response.setMessage("SUCCESSFULLY update (id : " + id + ")");
                response.setData(isDeleted);
                response.setStatus(HttpStatus.OK);
                response.setTime(new Timestamp(System.currentTimeMillis()));

            } else {
                response.setMessage("FAIL to update (id : " + id + ")");
                response.setData(null);
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setTime(new Timestamp(System.currentTimeMillis()));
            }
        } catch (Exception e) {
            response.setMessage("FAIL to update (id : " + id + ")");
            response.setData(bookDto);
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTime(new Timestamp(System.currentTimeMillis()));
        }

        return ResponseEntity.ok(response);


    }


    @GetMapping("/books")
    public ResponseEntity<BaseApiResponse<List<Book>>> getAll() {
        BaseApiResponse<List<Book>> response = new BaseApiResponse<>();
        System.out.println("kfakfka");
        List<Book> list = bookService.getAll();
        System.out.println(list);
        response.setData(list);
        response.setStatus(HttpStatus.OK);
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setMessage("You have found all books");
        return ResponseEntity.ok(response);
    }

    @GetMapping("/books/{id}")
    public ResponseEntity<BaseApiResponse<Book>> getOne(@PathVariable("id") int id) {
        BaseApiResponse<Book> response = new BaseApiResponse<>();
        try {
            Book book = bookService.getOne(id);
            if (book!=null) {
                response.setStatus(HttpStatus.OK);
                response.setMessage("you found a book");
                response.setData(book);
                response.setTime(new Timestamp((System.currentTimeMillis())));
                return ResponseEntity.ok(response);
            }else{
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setMessage("book not found");
                response.setData(null);
                response.setTime(new Timestamp((System.currentTimeMillis())));
                return ResponseEntity.ok(response);
            }
        } catch (Exception e) {
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setMessage(e.getMessage());
            response.setData(null);
            response.setTime(new Timestamp((System.currentTimeMillis())));
            return ResponseEntity.ok(response);
        }
    }

}
