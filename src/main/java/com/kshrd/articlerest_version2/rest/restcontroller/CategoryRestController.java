package com.kshrd.articlerest_version2.rest.restcontroller;

import com.kshrd.articlerest_version2.repository.dto.CategoryDto;
import com.kshrd.articlerest_version2.rest.request.CategoryRequestModel;
import com.kshrd.articlerest_version2.rest.response.BaseApiResponse;
import com.kshrd.articlerest_version2.service.impl.CategoryServiceImpl;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.List;

@RestController
public class CategoryRestController {
    private CategoryServiceImpl categoryService;

    @Autowired
    public void setCategoryService(CategoryServiceImpl categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping("/categories")
    public ResponseEntity<BaseApiResponse<CategoryRequestModel>> insert(
            @RequestBody CategoryRequestModel categoryRequestModel
    ) {
        BaseApiResponse<CategoryRequestModel> response = new BaseApiResponse<>();
        ModelMapper mapper = new ModelMapper();
        CategoryDto categoryDto = mapper.map(categoryRequestModel, CategoryDto.class);
        try {
            CategoryDto result = categoryService.insert(categoryDto);
            CategoryRequestModel result2 = mapper.map(result, CategoryRequestModel.class);
            response.setMessage("You have added category successfully");
            response.setData(result2);
            response.setStatus(HttpStatus.OK);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            response.setMessage("You have FAILED to add category ");
            response.setData(null);
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            return ResponseEntity.ok(response);
        }

    }

    @DeleteMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse> delete(
            @PathVariable("id") int id) {

        BaseApiResponse response = new BaseApiResponse();
        boolean isDeleted = categoryService.delete(id);

        if (isDeleted == true) {
            response.setMessage("SUCCESSFULLY delete (id : " + id + ")");
            response.setData("");
            response.setStatus(HttpStatus.OK);
            response.setTime(new Timestamp(System.currentTimeMillis()));
        } else {
            response.setMessage("FAIL to delete(id : " + id + ")");
            response.setData("");
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTime(new Timestamp(System.currentTimeMillis()));

        }
        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories")
    ResponseEntity<BaseApiResponse<List<CategoryDto>>> getAll() {
        BaseApiResponse<List<CategoryDto>> response = new BaseApiResponse<>();
        List<CategoryDto> list = categoryService.getAll();
        response.setData(list);
        response.setMessage("you have foound alll categories");
        response.setTime(new Timestamp(System.currentTimeMillis()));
        response.setStatus(HttpStatus.OK);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/categories/{id}")
    ResponseEntity<BaseApiResponse<CategoryDto>> getOne(@PathVariable("id") int id) {
        BaseApiResponse<CategoryDto> response = new BaseApiResponse<>();
        try {
            CategoryDto result = categoryService.getOne(id);
            if (result != null) {
                response.setData(result);
                response.setStatus(HttpStatus.OK);
                response.setTime(new Timestamp(System.currentTimeMillis()));
                response.setMessage("Found");
            } else {
                response.setData(null);
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setTime(new Timestamp(System.currentTimeMillis()));
                response.setMessage("Not Found");
            }

        } catch (Exception e) {
            response.setData(null);
            response.setStatus(HttpStatus.NOT_FOUND);
            response.setTime(new Timestamp(System.currentTimeMillis()));
            response.setMessage(e.getMessage());
        }
        return ResponseEntity.ok(response);

    }

    @PutMapping("/categories/{id}")
    public ResponseEntity<BaseApiResponse<CategoryDto>> update(
            @RequestBody CategoryDto categoryDto, @PathVariable("id") int id
    ) {
        BaseApiResponse<CategoryDto> response = new BaseApiResponse();
        try{
            CategoryDto isDeleted = categoryService.update(categoryDto,id);
            if (isDeleted != null){
                categoryDto.setId(id);
                response.setMessage("SUCCESSFULLY update (id : " + id + ")");
                response.setData(categoryDto);
                response.setStatus(HttpStatus.OK);
                response.setTime(new Timestamp(System.currentTimeMillis()));
            }else {
                response.setMessage("FAIL to update (id : " + id + ")");
                response.setData(null);
                response.setStatus(HttpStatus.NOT_FOUND);
                response.setTime(new Timestamp(System.currentTimeMillis()));
            }
        }catch(Exception e){
            response.setMessage("FAIL to update (id : " + id + ")");
            categoryDto.setId(id);
            response.setData(categoryDto);
            response.setStatus(HttpStatus.BAD_REQUEST);
            response.setTime(new Timestamp(System.currentTimeMillis()));
        }
        return ResponseEntity.ok(response);

    }

}
