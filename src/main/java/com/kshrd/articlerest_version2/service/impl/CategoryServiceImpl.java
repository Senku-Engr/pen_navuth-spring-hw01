package com.kshrd.articlerest_version2.service.impl;

import com.kshrd.articlerest_version2.repository.CategoryRepository;
import com.kshrd.articlerest_version2.repository.dto.BookDto;
import com.kshrd.articlerest_version2.repository.dto.CategoryDto;
import com.kshrd.articlerest_version2.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    private CategoryRepository categoryRepository;

    public CategoryServiceImpl() {
    }

    @Autowired
    public void setCategoryRepository(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    @Override
    public CategoryDto insert(CategoryDto categoryDto) {
        boolean isInserted = categoryRepository.insert(categoryDto);
        if (isInserted)
            return categoryDto;
        else
            return null;
    }

    @Override
    public boolean delete(int id) {
        return categoryRepository.delete(id);
    }

    @Override
    public List<CategoryDto> getAll() {
        return categoryRepository.getAll();
    }

    @Override
    public CategoryDto getOne(int id) {
        return categoryRepository.getById(id);
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto, int id) {
        boolean isUpdate = categoryRepository.update(categoryDto, id);
        if (isUpdate) {
            categoryDto.setId(id);
            return categoryDto;
        } else
            return null;
    }
}
