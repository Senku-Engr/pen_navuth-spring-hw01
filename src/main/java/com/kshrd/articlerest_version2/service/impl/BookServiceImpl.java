package com.kshrd.articlerest_version2.service.impl;

import com.kshrd.articlerest_version2.repository.BookRepository;
import com.kshrd.articlerest_version2.repository.dto.Book;
import com.kshrd.articlerest_version2.repository.dto.BookDto;
import com.kshrd.articlerest_version2.service.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService {

    BookRepository bookRepository;

    @Override
    public BookDto getById(int id) {
//        return bookRepository.getById(id);
        return null;
    }

    @Override
    public Book getOne(int id) {
        return bookRepository.getById(id);
    }

    @Override
    public List<Book> getAll() {
        return bookRepository.getAll();
    }

    @Autowired
    public void setBookRepository(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }


    @Override
    public boolean delete(int id) {
//        boolean isDeleted = bookRepository.delete(id);
        return bookRepository.delete(id);
//        System.out.println("isdelete : " + isDeleted);
//        if (isDeleted == false) {
//            return null;
//        } else {
//            System.out.println(id);
//            Book book = bookRepository.getById(id);
//            System.out.println(book); // error here
//            return book;
//        }
    }

//    @Override
//    public BookDto update(BookDto bookDto, int id) {
//        BookDto isUpdate = bookRepository.update(bookDto,id);
//        return null;
//    }


    @Override
    public BookDto update(BookDto bookDto, int id) {
        boolean isUpdate = bookRepository.update(bookDto,id);
        if (isUpdate){
            return  bookDto;
        }
       else
        {
            return null;
        }
    }

    @Override
    public BookDto insert(BookDto bookDto) {
        boolean isInserted = bookRepository.insert(bookDto);
        if (isInserted)
            return bookDto;
        else
            return null;
    }
}
