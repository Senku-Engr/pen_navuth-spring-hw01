package com.kshrd.articlerest_version2.service;

import com.kshrd.articlerest_version2.repository.dto.Book;
import com.kshrd.articlerest_version2.repository.dto.BookDto;

import java.util.List;

public interface BookService {
    BookDto insert(BookDto bookDto);
    boolean delete(int id);
    BookDto getById(int id);
    BookDto update(BookDto bookDto,int id);
    List<Book> getAll();
    Book getOne(int id);
}
