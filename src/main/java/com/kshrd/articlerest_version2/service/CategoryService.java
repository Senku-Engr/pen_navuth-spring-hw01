package com.kshrd.articlerest_version2.service;

import com.kshrd.articlerest_version2.repository.dto.CategoryDto;

import java.util.List;

public interface CategoryService {
    CategoryDto insert(CategoryDto categoryDto);
    boolean delete(int id);
    List<CategoryDto> getAll();
    CategoryDto getOne(int id);
    CategoryDto update(CategoryDto categoryDto, int id);
}
