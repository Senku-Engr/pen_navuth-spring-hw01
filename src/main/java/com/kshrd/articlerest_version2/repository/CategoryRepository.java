package com.kshrd.articlerest_version2.repository;

import com.kshrd.articlerest_version2.repository.dto.Book;
import com.kshrd.articlerest_version2.repository.dto.CategoryDto;
import com.kshrd.articlerest_version2.repository.provider.CategoryProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository {
    @InsertProvider(type = CategoryProvider.class, method = "insertCategorySql")
    boolean insert(CategoryDto categoryDto);

    @DeleteProvider(type = CategoryProvider.class, method = "deleteCategorySql")
    boolean delete(int id);

    @Select("SELECT * FROM tb_categories")
    List<CategoryDto> getAll();

    @Select("select * from tb_categories where id = #{id}")
    CategoryDto getById(@Param("id") int id);

    @UpdateProvider(type = CategoryProvider.class, method = "updateSql")
    boolean update(CategoryDto categoryDto, @Param("id") int id);
}
