package com.kshrd.articlerest_version2.repository.dto;

public class BookDto {
    private int id,category_id;
    private String author,title,description,thumbnail;

    @Override
    public String toString() {
        return "BookDto{" +
                "id=" + id +
                ", category_id=" + category_id +
                ", author='" + author + '\'' +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public BookDto(int id, int category_id, String author, String title, String description, String thumbnail) {
        this.id = id;
        this.category_id = category_id;
        this.author = author;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
    }
}
