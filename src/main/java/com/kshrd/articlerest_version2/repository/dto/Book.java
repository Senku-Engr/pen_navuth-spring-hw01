package com.kshrd.articlerest_version2.repository.dto;

public class Book {
    private int id;
    private String title;
    private String description;
    private String thumbnail;
    private String author;
    private int category_id;

    public Book(int id, String title, String description, String thumbnail, String author, int categoryDto) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.thumbnail = thumbnail;
        this.author = author;
        this.category_id = categoryDto;
    }
public Book(){}
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public int getCategoryDto() {
        return category_id;
    }

    public void setCategoryDto(int categoryDto) {
        this.category_id = categoryDto;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", description='" + description + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", author='" + author + '\'' +
                ", categoryDto=" + category_id +
                '}';
    }
}
