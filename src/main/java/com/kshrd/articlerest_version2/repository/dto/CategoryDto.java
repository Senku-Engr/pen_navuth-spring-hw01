package com.kshrd.articlerest_version2.repository.dto;

import com.kshrd.articlerest_version2.service.CategoryService;

public class CategoryDto {
    private int id;
    private String title;
public CategoryDto(){}

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", title='" + title + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public CategoryDto(int id, String title) {
        this.id = id;
        this.title = title;
    }
}
