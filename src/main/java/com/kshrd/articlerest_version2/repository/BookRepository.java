package com.kshrd.articlerest_version2.repository;

import com.kshrd.articlerest_version2.repository.dto.Book;
import com.kshrd.articlerest_version2.repository.dto.BookDto;
import com.kshrd.articlerest_version2.repository.dto.RoleDto;
import com.kshrd.articlerest_version2.repository.dto.UserDto;
import com.kshrd.articlerest_version2.repository.provider.BookProvider;
import com.kshrd.articlerest_version2.repository.provider.UserProvider;
import org.apache.ibatis.annotations.*;
import org.postgresql.util.PSQLException;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookRepository {

    @InsertProvider(type = BookProvider.class, method = "insertBookSql")
    boolean insert(BookDto bookDto);

    @DeleteProvider(type = BookProvider.class, method = "deleteBookSql")
    boolean delete(int id);

//    @SelectProvider(type = BookProvider.class, method = "getBookByIdSql")
//    @Select("SELECT * FROM tb_books")
//    @Results({
//            @Result(column = "id", property = "id"),
//            @Result(column = "title", property = "title"),
//            @Result(column = "thumbnail", property = "thumbnail"),
//            @Result(column = "author", property = "author"),
//            @Result(column = "category_id", property = "category_id"),
//
//    })
//    @Select("select * from tb_books where id = #{id}")
//    BookDto getById(@Param("id") int id);

    @Select("select * from tb_books where id = #{id}")
    Book getById(@Param("id") int id);

//
    @UpdateProvider(type = BookProvider.class, method = "updateSql")
    boolean update(BookDto bookDto, @Param("id") int id);

//    @SelectProvider(type = BookProvider.class, method = "selectAll")

//    @Select("SELECT * FROM tb_books")
//    List<BookDto> seltectAll();

    @Select("SELECT * FROM tb_books")
    List<Book> getAll();

}
