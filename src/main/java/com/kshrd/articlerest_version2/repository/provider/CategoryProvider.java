package com.kshrd.articlerest_version2.repository.provider;

import com.kshrd.articlerest_version2.repository.dto.BookDto;
import com.kshrd.articlerest_version2.repository.dto.CategoryDto;
import org.apache.ibatis.jdbc.SQL;

public class CategoryProvider {
    public String insertCategorySql(){
        return new SQL(){{
            INSERT_INTO("tb_categories");
            VALUES("title","#{title}");
        }}.toString();
    }

    public String deleteCategorySql(int id){
        return new SQL(){{
            DELETE_FROM("tb_categories");
            WHERE("id = '"+id+ "'");
        }}.toString();
    }

    public String updateSql(CategoryDto categoryDto, int id){
        return new SQL(){{
            UPDATE("tb_categories");
            SET("title=#{categoryDto.title}");
            WHERE("id = "+ id );
        }}.toString();
    }
}
