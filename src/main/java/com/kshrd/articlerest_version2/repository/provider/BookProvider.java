package com.kshrd.articlerest_version2.repository.provider;

import com.kshrd.articlerest_version2.repository.dto.BookDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.jdbc.SQL;

public class BookProvider {
    public String insertBookSql(){
        return new SQL(){{
            INSERT_INTO("tb_books");
            VALUES("title","#{title}");
            VALUES("author", "#{author}");
            VALUES("description","#{description}");
            VALUES("thumbnail", "#{thumbnail}");
            VALUES("category_id","#{category_id}");
        }}.toString();

    }
    public String deleteBookSql(int id){
        return new SQL(){{
            DELETE_FROM("tb_books");
            WHERE("id = '"+id+ "'");
        }}.toString();
    }

    public String getBookByIdSql(@Param("id") int id){
        System.out.println("id get : " + id);
        String SQL = new SQL(){{
            SELECT("*");
            FROM("tb_books");
            WHERE("id = "+id );
        }}.toString();
        System.out.println(SQL);
        return SQL;
    }

    public String updateSql(BookDto bookDto,int id){
        return new SQL(){{
            UPDATE("tb_books");
            SET("title=#{bookDto.title}");
            SET("author=#{bookDto.author}");
            SET("description=#{bookDto.description}");
            SET("thumbnail=#{bookDto.thumbnail}");
            SET("category_id=#{bookDto.category_id}");
            WHERE("id = "+ id );
        }}.toString();
    }

    public String selectAll(){
        return new SQL(){{
            SELECT("*");
            FROM("tb_books");
        }}.toString();
    }
}
